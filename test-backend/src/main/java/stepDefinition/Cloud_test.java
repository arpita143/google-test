package stepDefinition;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Cloud_test{

	
    @Then("^I will get data back$")
    public void callStoredProcedures() throws SQLNonTransientConnectionException 
    ,IOException, SQLException {
   
    	String instanceConnectionName = "mona-lisa-338219:us-central1:monalisa";
    	String databaseName = "db_tuscany";


    	String IP_of_instance = "35.239.76.158";
    	String username = "testuser1";
    	String password = "tuscany";

    	String jdbcUrl = String.format("jdbc:mysql://%s/%s?cloudSqlInstance=%s" + 
    	        "&useSSL=false",
    	        IP_of_instance, databaseName, instanceConnectionName);

    	String Url = "jdbc:mysql://"+IP_of_instance+"/" +databaseName+"?useSSL=false";

    	try (Connection connection =
    	DriverManager.getConnection(jdbcUrl, username, password)) {

    	String query = "call GetCustomerLevel(141, @level)";
    	CallableStatement cb = connection.prepareCall(query);

    	ResultSet rs = cb.executeQuery();

    	while (rs.next()) {
    	System.out.println("Results: " + rs.getString("level"));
    	}

    	query = "call GetOfficeByCountry()";
    	cb = connection.prepareCall(query);

    	rs = cb.executeQuery();

    	while (rs.next()) {
    	System.out.println("Results: " + rs.getString("country"));
    	}

    	query = "call GetOrderCountByStatus()";
    	cb = connection.prepareCall(query);

    	rs = cb.executeQuery();

    	while (rs.next()) {
    	System.out.println("Results: " + rs.getString("total"));
    	}

    	} catch (SQLException e) {
    	e.printStackTrace();
    	}

		if (test_status) {
		    TestResult = "Able to get results.\n";
		    screenShot();
			Assert.assertTrue(true);
		} else {
		    TestResult = TestResult + "\n" + "Failed";
		    screenShot();
		    Assert.assertTrue(false);
			}
		}
    
   
}