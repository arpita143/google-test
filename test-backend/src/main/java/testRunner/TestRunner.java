package testRunner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.sikuli.script.ImagePath;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import commons.Commons;
import config.FileHelper;
import config.JsonData;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/main/java/Features" }, glue = { "stepDefinition" }, tags = {
	"@testWrapup" }, monochrome = true)

public class TestRunner extends Commons {

    @BeforeClass
    public static void testRunSetup() throws Exception {
	
	startTime = System.currentTimeMillis();
	ImagePath.add("testRunner.TestRunner/images");
	FileHelper.createFolder(suite);
	htmlReporter = new ExtentHtmlReporter(FileHelper.directoryName + "/" + suite + " Report.html");
	extent = new ExtentReports();
	extent.attachReporter(htmlReporter);
    }

    @AfterClass
    public static void testRunWrapup() throws Exception {
	endTime = System.currentTimeMillis();
	totalTime = endTime - startTime;
	System.out.println(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(totalTime),
		TimeUnit.MILLISECONDS.toSeconds(totalTime)
			- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))));
    }
}
